#!/bin/bash
project=$1
mkdir $project
subfolders (){
k=$1
shift
for j in $@
do
mkdir $project/$k/$j
done
}
l=(asiakirjat kuvat muut)
for i in ${l[@]}
do
mkdir $project/$i
done
subfolders asiakirjat pdf odf
subfolders kuvat png svg
