#!/bin/bash
# tehnyt @alukortti
INTERVAL='5%'

case $1 in
    "up")
        for SINK in $(pactl list sinks | grep 'Sink #' | cut -b7-)
        do
          pactl set-sink-volume $SINK +$INTERVAL
        done
        ;;
    "down")
        for SINK in $(pactl list sinks | grep 'Sink #' | cut -b7-)
        do
          pactl set-sink-volume $SINK -$INTERVAL
        done
        ;;
esac

